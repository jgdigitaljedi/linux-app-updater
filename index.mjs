import config from './config.json' assert { type: 'json' };
import { deleteNewApps } from './util/deleteDownloaded.mjs';
import copyConfigToUpdated from './util/copyConfigToUpdated.mjs';
import checkBambu from './src/checkBambuStudio.mjs';
import checkStreamdeck from './src/checkStreamdeck.mjs';
import checkStarship from './src/checkStarship.mjs';
import checkNvm from './src/checkNvm.mjs';
import checkDeno from './src/checkDeno.mjs';
import checkPinetime from './src/checkPinetime.mjs';
import check432HzBatchConvertor from './src/check432HzBatch.mjs';
import checkGoverlay from './src/checkGoverlay.mjs';
import checkMangohud from './src/checkMangohud.mjs';
import checkSdkman from './src/checkSdkman.mjs';
import checkThefuck from './src/checkThefuck.mjs';
import checkGamemode from './src/checkGamemode.mjs';
import checkVkbasalt from './src/checkVkbasalt.mjs';
import checkLmStudio from './src/checkLmStudio.mjs';
import checkOrcaSlicer from './src/checkOrcaSlicer.mjs';

(async () => {
  // setup updatedConfig
  copyConfigToUpdated();

  // delete downloaded apps first
  await deleteNewApps();

  // check for Bambu Studio (for Fedora) updates
  if (config.bambuStudio_Fedora) {
    await checkBambu();
  }

  // check for Bambu Studio (for Ubuntu) updates
  if (config.bambuStudio_Ubuntu) {
    await checkBambu(true);
  }

  // check for StreamDeck UI updates
  if (config.streamdeckGUI) {
    await checkStreamdeck();
  }

  // check for Starship updates
  if (config.starship) {
    await checkStarship();
  }

  // check for nvm updates
  if (config.nvm) {
    await checkNvm();
  }

  // check for Deno updates
  if (config.deno) {
    await checkDeno();
  }

  // check for PineTime updates
  if (config.pinetime) {
    await checkPinetime();
  }

  // check for 432Hz Batch Convertor updates
  if (config.batch432) {
    await check432HzBatchConvertor();
  }

  // check for Goverlay updates
  if (config.goverlay) {
    await checkGoverlay();
  }

  // check for MangoHud updates
  if (config.mangohud) {
    await checkMangohud();
  }

  // check for SDKman updates
  if (config.sdkman) {
    await checkSdkman();
  }

  // check for thefuck updates
  if (config.thefuck) {
    await checkThefuck();
  }

  // check for GameMode updates
  if (config.gamemode) {
    await checkGamemode();
  }

  // check for VKBasalt updates
  if (config.vkbasalt) {
    await checkVkbasalt();
  }

  // check for LM Studio updates
  if (config.lmstudio) {
    await checkLmStudio();
  }

  // check for Orca Slicer updates
  if (config.orcaSlicer) {
    await checkOrcaSlicer();
  }
})();
