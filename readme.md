# linux-app-updater

## What is this for

We've got native packages, flatpaks, snaps, etc. The problem is that sometimes we like to use AppImage's, build from source, install using pip, run an installation script that uses curl to download a directory and then install it, etc to also install apps. I tend to just write updater scripts when working on any Linux machine and wanted a way to check for updates for the apps that don't have a simple path to updating, so I wrote this little Node script to get the job done and will add it to my updater scripts.

Also, it is worth noting that a few of these like Mangohud and GOveraly are usually better installed via your package manager. I'm currently running Pop_OS! 22.04 (for now) and the version provided by the package manager do not work. From what I've read online, this is an issue with a lot of Ubuntu based distros. Anyway, always seek out the best path for installation for your distro and this script can check for updates and even update some of the things that don't have a simpler path.

---

## Supported apps

This is a short list as it only has checks for the apps I need at the moment, but the list will keep growing.

- [Bambu Studio](https://github.com/bambulab/BambuStudio) - Downloads updates as AppImage (Fedora or Ubuntu version determined by config key)
- [Streamdeck-UI](https://github.com/timothycrosley/streamdeck-ui)- Installs updates using pip
- [Starship](https://github.com/starship/starship) - Installs updates using shell scripting
- [nvm](https://github.com/nvm-sh/nvm) - Installs updates using shell scripting
- [Deno](https://github.com/denoland/deno) - Installs updates using shell scripting
- [PineTime watch firmware](https://github.com/InfiniTimeOrg/InfiniTime) - Downloads updates as a .zip archive
- [432Hz Batch Convertor](https://github.com/mysteryx93/HanumanInstituteApps) - Downloads updates as AppImage
- [Goverlay](https://github.com/benjamimgois/goverlay) - Downloads updates as tarball to do whatever with
- [MangoHud](https://github.com/flightlessmango/MangoHud) - Downloads updates as tarball to do whatever with
- [SDKman](https://github.com/sdkman/sdkman-cli) - Installs updates using shell script (uses zsh over bash)
- [thefuck](https://github.com/nvbn/thefuck) - Installs updates using pip
- [GameMode](https://github.com/FeralInteractive/gamemode) - Downloads updates as .tar.xz archive
- [vkBasalt](https://github.com/DadSchoorse/vkBasalt) - Downloads updates as .tar.gz archive
- [LM Studio](https://lmstudio.ai/) - Downloads updates as .AppImage

Here is a brief explanation of what each note beside each app above means in the event that it isn't immediately obvious:

- "Downloads updates as AppImage" - If a new version is detected in the app's repository, the new AppImage is downloaded to the /downloads folder of this project for you to move and do whatever with.
- "Installs updates using pip" - If a new version is detected in the app's repository, a pip update command is run to update that app using pip. The general format of this command is `python3 -m pip install <appName> -U`.
- "Installs updates using shell scripting" - If a new version is detected in the app's repository, the app is updated using shell scripting. This is usually the case when the app is something installed with a curl command and a `| sh` or `| bash` at the end to execute the download.
- "Installs updates using shell script" - If a new version is detected in the app's repository, the app is updated using a shell script. This is usually the case when the update command needs to be run with env variables or in zsh instead of bash for my setup.
- "Downloads updates as a .zip archive" - If a new version is detected in the app's repository, the .zip archive is downloaded. At the moment, the only thing this happens for is the PineTime firmware updates as I own a PineTime and wanted to incorporate this check so I know when new firmware is available. Obviously, nothing is installed in this scenario as I have to copy the downloaded firmare to my phone to be installed onto my watch via GadgetBridge.
- "Downlads updates as tarball to do whatever with" - If a new version is detected in the app's repository, it is downloaded as a tarbell for you to continue the installation. This isn't as convenient as scripting the decompression and installation, but these installation process for these types of app sometimes change and there might be new dependencies to install so it seemed best to leave the manual installation to the user.

---

## General setup

The `config.json` file in the root of the project has to be setup with your software and versions. If you have questions about how to setup your config, you can checkout the config files in the `testConfigs` directory. The `allConfig.json` file has a config entry for every supported app/tool and is used by me to test my scripts and make sure they work so this is probably the best example as it shows the complete list of software supported and how the versions are formatted. The `myConfig.json` is what I use is usually what I restore the `config.json` file to before I push new changes.

To get the script ready, clone it to a directory of your choosing, navigate into that directory, and run `npm install` to install the dependencies. _Note this this tool has a dependency of Node version 16 or higher to work!_ If you do not know what Node JS is, this tool isn't for you.

### Config

The config file itself is relatively simple to setup. There are keys for each app that can be updated and the value is an object with 2 additional keys; "version" for the current version you have installed and "updateConfig" which determines whether or not the script automatically updates your root config file after updating. To me, it makes sense to have the root config file automatically updated for the apps that are actually fully updated by this script and not for the apps that just have an AppImage or source files downloaded as most people are going to handle this in different ways.

Here is a table of possible keys and their default values (all of which can be changed to suit your needs):

|        **App**        |                   **Key**                    | **"version"**  | **"updateConfig"** |          **installation**           |
| :-------------------: | :------------------------------------------: | :------------: | :----------------: | :---------------------------------: |
|     Bambu Studio      | "bambuStudio_Ubuntu" or "bambuStudio_Fedora" | "v01.10.01.50" |       false        |         Downloads AppImage          |
| Streamdeck Linux GUI  |               "streamdeckGUI"                |    "v4.1.3"    |       false        |            Downloads zip            |
|       Starship        |                  "starship"                  |   "v1.22.1"    |        true        | Installs via curl and shell command |
|          Nvm          |                    "nvm"                     |   "v0.40.1"    |        true        | Installs via curl and shell command |
|         Deno          |                    "deno"                    |    "v2.1.10"    |        true        | Installs via curl and shell command |
|   PineTime firmware   |                  "pinetime"                  |    "1.15.0"    |       false        |            Downloads zip            |
| 432Hz Batch Converter |                  "batch432"                  |    "v3.3.2"    |       false        |         Downloads AppImage          |
|       Goverlay        |                  "goverlay"                  |     "1.2"      |       false        |  Downloads archive of source files  |
|       MangoHud        |                  "mangohud"                  |    "v0.8.0"    |       false        |  Downloads archive of source files  |
|        SDKman         |                   "sdkman"                   |    "5.19.0"    |        true        |   Installs using selfupdate (zsh)   |
|        theFuck        |                  "thefuck"                   |     "3.32"     |        true        |          Installs via pip           |
|       GameMode        |                  "gamemode"                  |    "1.8.2"     |       false        |          Downloads tarball          |
|       vkBasalt        |                  "vkbasalt"                  |  "v0.3.2.10"   |       false        |          Downloads tarball          |
|       LM Studio       |                  "lmstudio"                  |    "0.3.9"     |       false        |         Downloads AppImage          |
|      Orca Slicer      |                 "orcaSlicer"                 |    "v2.2.0"    |       false        |         Downloads AppImage          |

---

## Running

Once this script is cloned and the npm dependencies are installed, you can simply run `npm start` to run the script. Conversely, you can alias the script like I do and run it with a single command from anywhere. Any updates will log to the console and be downloaded to the `downloads` folder. Also, to make it simpler to update your config file, you can find a copy of your config with all of the latest versions of everything in `data/updatedConfig.json` after this script is run. If you updated all of your items after running this tool, you can simply copy the contents of that file to `config.json` in the root to instantly update your config.

---

## Disclaimer

I made this tool for myself. That said, there are things in here that might need to be changed to be executed on a different machine. Paths could be different, my shell script for SDKman uses zsh over bash to load my env variables, etc. If you want to use this script, just make sure you have it properly configured for you own system. I will say that I use this on my gaming PC, laptop, and work laptop without issue, but my personal machines are running Pop_OS! and my work laptop runs Ubuntu (not my choice) so they are all running the same core OS in essence.

This tool does install some software automatically. I might make that optional so that this tool will eventually just alert you of an update when one is available so you can manually install it, but that isn't how it works right now. That said, you should always exercise caution when using someone else's code as your particular setup might warrant variations of these commands or different commands all together.

Note that I am running Pop_OS! 22.04 on my dev and gaming machines and this tool works perfectly for me. That said, while the commands are pretty much distro agnostic at the moment, paths and other things might vary slightly for other distros. I would assume that this tools would work for any distro, but I guess I can make the claim that since I am testing it on Pop_OS!, that distro and probably any other Ubuntu or even Debian derivative have tested/proven compatibility.

## Dependecies

Being that this is a node script, node.js installed is obviously the main dependency and the node modules used to execute the script should be installed as well. Below is a complete list of other dependencies you may need depending on what apps you use this to update:

| **Dependency** |          **Website**          |
| :------------: | :---------------------------: |
| Node.js (v18+) |     https://nodejs.org/en     |
|     Python     |    https://www.python.org/    |
|      Pip       | https://pypi.org/project/pip/ |
|      Zsh       |  https://zsh.sourceforge.io/  |
