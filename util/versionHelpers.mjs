// matches version formatted like "v1.01" or "v2.99" or "v3.01b2"
export const matchVVersion = (str) => {
  return str.match(/v[0-9].[0-9][0-9]([a-z][0-9])?/)[0];
};
