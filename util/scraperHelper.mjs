import fetch from 'node-fetch';
import * as cheerio from 'cheerio';

export async function scrapeSite(url) {
  try {
    const fwPage = await fetch(url);
    const body = await fwPage.text();
    const $ = cheerio.load(body);
    return $;
  } catch (error) {
    return { error };
  }
}
