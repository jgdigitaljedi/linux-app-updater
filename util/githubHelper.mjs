import { Octokit } from '@octokit/rest';

export const getGithubData = async (owner, repo, returnAssetsArr) => {
  try {
    const octokit = new Octokit({ auth: process.env.GITHUB_KEY });
    const releases = await octokit.request('GET /repos/{owner}/{repo}/releases/latest', {
      owner,
      repo,
      headers: {
        'X-GitHub-Api-Version': '2022-11-28'
      }
    });

    const data = releases.data;
    const latest = data.tag_name;
    // const dlLink = data.assets[0].browser_download_url;
    const dlLink = !!data?.assets?.length ? data.assets[0].browser_download_url : data.zipball_url;

    return { latest, dlLink, assets: returnAssetsArr ? releases.data.assets : undefined };
  } catch (error) {
    return { error };
  }
};

const getTaggedVersion = (releases) => {
  if (releases[0].name === 'latest' && releases.length > 1) {
    return releases[1];
  }
  return releases[0];
};

export const getGhTags = async (gData) => {
  try {
    const octokit = new Octokit({ auth: process.env.GITHUB_KEY });
    const releases = await octokit.request('GET /repos/{owner}/{repo}/tags', {
      owner: gData.owner,
      repo: gData.repo,
      headers: {
        'X-GitHub-Api-Version': '2022-11-28'
      }
    });
    const last = gData.getTagged ? getTaggedVersion(releases.data) : releases.data[0];
    const latest = last.name;
    const dlLink = last.zipball_url;

    return { latest, dlLink };
  } catch (error) {
    return { error };
  }
};
