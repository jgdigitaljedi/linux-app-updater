import chalk from 'chalk';
import { readFileSync, writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';
import config from '../config.json' assert { type: 'json' };

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const writeUpdatedConfig = async (key, version) => {
  try {
    const updatedConfigPath = join(__dirname, '../data/updatedConfig.json');
    const updatedConfig = readFileSync(updatedConfigPath, 'utf-8');
    const parsed = JSON.parse(updatedConfig);
    parsed[key].version = version;
    writeFileSync(updatedConfigPath, JSON.stringify(parsed, null, 2), 'utf-8');

    // update root config
    if (config[key].updateConfig) {
      const copy = { ...config };
      copy[key].version = version;
      const rootConfigPath = join(__dirname, '../config.json');
      writeFileSync(rootConfigPath, JSON.stringify(copy, null, 2), 'utf-8');
    }

    return Promise.resolve();
  } catch (error) {
    console.log(chalk.red.bold('Error writing updatedConfig.json: ', error));
    return Promise.reject(error);
  }
};
