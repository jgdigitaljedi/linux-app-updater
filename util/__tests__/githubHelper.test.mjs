import { getGithubData } from '../githubHelper.mjs';
import allConfig from '../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../data/dlLinkList.json' assert { type: 'json' };
import genericGhApps from '../constants/genericGhApps.mjs';
import { genericGhUpdate } from '../genericGhUpdate.mjs';

// just doing loose checks to make sure these props come back from the call.
// additional logic will be tested in other files.

describe('github endpoints', () => {
  it('should get a response for the Bambu Studio (Fedora) GitHub call', async () => {
    const { latest, dlLink } = await getGithubData(
      genericGhApps.bambuStudio_Fedora.owner,
      genericGhApps.bambuStudio_Fedora.repo
    );
    expect(latest).toBe(allConfig.bambuStudio_Fedora.version);
    expect(dlLink).toBe(dlLinks.bambuStudio_Fedora);
  });

  it('should get a response for the Bambu Studio (Ubuntu) GitHub call', async () => {
    const { latest, assets } = await getGithubData(
      genericGhApps.bambuStudio_Ubuntu.owner,
      genericGhApps.bambuStudio_Ubuntu.repo,
      true
    );
    expect(latest).toBe(allConfig.bambuStudio_Ubuntu.version);
    expect(assets[1].browser_download_url).toBe(dlLinks.bambuStudio_Ubuntu);
  });

  it('should get a response for the StreamDeck-GUI GitHub call', async () => {
    const { latest, dlLink } = await genericGhUpdate(genericGhApps.streamdeckGUI, true);
    expect(latest).toBe(allConfig.streamdeckGUI.version);
    expect(dlLink).toBe(dlLinks.streamdeckGUI);
  });

  it('should get a response for the Starship GitHub call', async () => {
    const { latest, dlLink } = await genericGhUpdate(genericGhApps.starship, true);
    console.log('latest', latest);
    expect(latest).toBe(allConfig.starship.version);
    expect(dlLink).toBe(dlLinks.starship);
  });

  it('should get a response for the nvm GitHub call', async () => {
    const { latest, dlLink } = await genericGhUpdate(genericGhApps.nvm, true);
    expect(latest).toBe(allConfig.nvm.version);
    expect(dlLink).toBe(dlLinks.nvm);
  });

  it('should get a response for the Deno GitHub call', async () => {
    const { latest, dlLink } = await genericGhUpdate(genericGhApps.deno, true);
    expect(latest).toBe(allConfig.deno.version);
    expect(dlLink).toBe(dlLinks.deno);
  });

  it('should get a response for the PineTime firmware GitHub call', async () => {
    const { latest } = await getGithubData(
      genericGhApps.pinetime.owner,
      genericGhApps.pinetime.repo
    );
    expect(latest).toBe(allConfig.pinetime.version);
  });

  it('should get a response for the 432Hz Batch GitHub call', async () => {
    const { latest } = await getGithubData(
      genericGhApps.batch432.owner,
      genericGhApps.batch432.repo
    );
    expect(latest).toBe(allConfig.batch432.version);
  });

  it('should get a response for the Goverlay GitHub call', async () => {
    const { latest, dlLink } = await getGithubData(
      genericGhApps.goverlay.owner,
      genericGhApps.goverlay.repo
    );
    expect(latest).toBe(allConfig.goverlay.version);
    expect(dlLink).toBe(dlLinks.goverlay);
  });

  it('should get a response for the MangoHud GitHub call', async () => {
    const { latest, dlLink } = await getGithubData(
      genericGhApps.mangohud.owner,
      genericGhApps.mangohud.repo
    );
    expect(latest).toBe(allConfig.mangohud.version);
    expect(dlLink).toBe(dlLinks.mangohud);
  });

  it('should get a response for the SDKman GitHub call', async () => {
    const { latest } = await getGithubData(
      genericGhApps.sdkman.owner,
      genericGhApps.sdkman.repo,
      true
    );
    expect(latest).toBe(allConfig.sdkman.version);
  });

  it('should get a response for the thefuck GitHub call', async () => {
    const { latest, dlLink } = await genericGhUpdate(genericGhApps.thefuck, true);
    expect(latest).toBe(allConfig.thefuck.version);
    expect(dlLink).toBe(dlLinks.thefuck);
  });

  it('should get a response for the GameMode GitHub call', async () => {
    const { latest } = await getGithubData(
      genericGhApps.gamemode.owner,
      genericGhApps.gamemode.repo
    );
    expect(latest).toBe(allConfig.gamemode.version);
  });

  it('should get a response for the VKBasalt GitHub call', async () => {
    const { latest } = await getGithubData(
      genericGhApps.vkbasalt.owner,
      genericGhApps.vkbasalt.repo
    );
    expect(latest).toBe(allConfig.vkbasalt.version);
  });

  it('should get a response for the Orca Slicer GitHub call', async () => {
    const { latest } = await getGithubData(
      genericGhApps.orcaSlicer.owner,
      genericGhApps.orcaSlicer.repo
    );
    expect(latest).toBe(allConfig.orcaSlicer.version);
  });
});
