import { readFileSync, writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const copyConfigToUpdated = () => {
  const updatedConfigPath = join(__dirname, '../data/updatedConfig.json');
  const current = readFileSync(join(__dirname, '../config.json'), 'utf-8');
  writeFileSync(updatedConfigPath, current, 'utf-8');
};

export default copyConfigToUpdated;
