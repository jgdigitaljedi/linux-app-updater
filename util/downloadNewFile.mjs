import { pipeline } from 'node:stream';
import { promisify } from 'node:util';
import fetch from 'node-fetch';
import chalk from 'chalk';
import { createWriteStream } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const downloadFw = async (ed, filePath, errorMessage) => {
  const streamPipeline = promisify(pipeline);
  const response = await fetch(ed.link);
  if (!response.ok) {
    console.log(chalk.red(errorMessage));
    return Promise.resolve();
  }
  await streamPipeline(response.body, createWriteStream(join(__dirname, filePath)));
};
