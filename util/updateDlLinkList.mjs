import chalk from 'chalk';
import { readFileSync, writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const updateDlLinkList = async (key, link) => {
  try {
    const updatedConfigPath = join(__dirname, '../data/dlLinkList.json');
    const updatedConfig = readFileSync(updatedConfigPath, 'utf-8');
    const parsed = JSON.parse(updatedConfig);
    parsed[key] = link;
    writeFileSync(updatedConfigPath, JSON.stringify(parsed, null, 2), 'utf-8');
    return Promise.resolve();
  } catch (error) {
    console.log(chalk.red.bold('Error writing dlLinkList.json: ', error));
    return Promise.reject(error);
  }
};
