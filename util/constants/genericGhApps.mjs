const genericGhApps = {
  bambuStudio_Fedora: {
    owner: 'bambulab',
    repo: 'BambuStudio',
    configKey: 'bambuStudio_Fedora',
    itemName: 'BambuStudio',
    filePathMid: 'BambuStudio_',
    ext: '.AppImage',
    term: 'fedora'
  },
  bambuStudio_Ubuntu: {
    owner: 'bambulab',
    repo: 'BambuStudio',
    configKey: 'bambuStudio_Ubuntu',
    itemName: 'BambuStudio',
    filePathMid: 'BambuStudio_',
    ext: '.AppImage',
    term: 'ubuntu'
  },
  streamdeckGUI: {
    owner: 'streamdeck-linux-gui',
    repo: 'streamdeck-linux-gui',
    configKey: 'streamdeckGUI',
    itemName: 'Source code (zip)',
    filePathMid: 'StreamDeckGUI_',
    ext: '.zip'
    // updateCmd: { type: 'pip', name: 'streamdeck-ui' },
    // processName: 'streamdeck',
    // startCmd: 'streamdeck & disown'
  },
  starship: {
    owner: 'starship',
    repo: 'starship',
    configKey: 'starship',
    itemName: 'Starship',
    filePathMid: 'Starship_',
    ext: null,
    updateCmd: { type: 'shell', name: 'sh -c "$(curl -sS https://starship.rs/install.sh)" -y -f' },
    sudo: true
  },
  nvm: {
    owner: 'nvm-sh',
    repo: 'nvm',
    configKey: 'nvm',
    itemName: 'nvm',
    filePathMid: 'nvm_',
    ext: null,
    updateCmd: {
      type: 'shellArg',
      beforeArg: 'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/',
      afterArg: '/install.sh | bash'
    }
  },
  deno: {
    owner: 'denoland',
    repo: 'deno',
    configKey: 'deno',
    itemName: 'Deno',
    filePathMid: 'Deno_',
    ext: null,
    updateCmd: {
      type: 'shell',
      name: 'curl -fsSL https://deno.land/x/install/install.sh | sh'
    }
  },
  pinetime: {
    owner: 'InfiniTimeOrg',
    repo: 'InfiniTime',
    configKey: 'pinetime',
    itemName: 'PineTime',
    filePathMid: 'PineTime_',
    ext: '.zip'
  },
  batch432: {
    owner: 'mysteryx93',
    repo: 'HanumanInstituteApps',
    configKey: 'batch432',
    itemName: '432Hz Batch Convertor',
    filePathMid: 'Batch432_',
    ext: '.AppImage'
  },
  goverlay: {
    owner: 'benjamimgois',
    repo: 'goverlay',
    configKey: 'goverlay',
    itemName: 'Goverlay',
    filePathMid: 'Goverlay_',
    ext: '.tar.gz'
  },
  mangohud: {
    owner: 'flightlessmango',
    repo: 'MangoHud',
    configKey: 'mangohud',
    itemName: 'MangoHud',
    filePathMid: 'MangoHud_',
    ext: '.tar.gz'
  },
  sdkman: {
    owner: 'sdkman',
    repo: 'sdkman-cli',
    configKey: 'sdkman',
    itemName: 'SDKman',
    filePathMid: 'SDKman_',
    ext: null,
    updateCmd: {
      type: 'shellScript',
      name: './util/updateSdkman.sh'
    },
    getTagged: true
  },
  thefuck: {
    owner: 'nvbn',
    repo: 'thefuck',
    configKey: 'thefuck',
    itemName: 'thefuck',
    filePathMid: 'thefuck_',
    ext: null,
    updateCmd: { type: 'pip', name: 'thefuck' }
  },
  gamemode: {
    owner: 'FeralInteractive',
    repo: 'gamemode',
    configKey: 'gamemode',
    itemName: 'GameMode',
    filePathMid: 'GameMode_',
    ext: '.xz'
  },
  vkbasalt: {
    owner: 'DadSchoorse',
    repo: 'vkBasalt',
    configKey: 'vkbasalt',
    itemName: 'VKBasalt',
    filePathMid: 'VKBasalt_',
    ext: '.gz'
  },
  orcaSlicer: {
    owner: 'SoftFever',
    repo: 'OrcaSlicer',
    configKey: 'orcaSlicer',
    itemName: 'OrcaSlicer',
    filePathMid: 'OrcaSlicer_',
    ext: '.AppImage',
    term: 'Linux_V'
  }
};

export default genericGhApps;
