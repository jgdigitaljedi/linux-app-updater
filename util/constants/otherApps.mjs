const otherApps = {
  lmstudio: {
    configKey: 'lmstudio',
    itemName: 'LM Studio',
    filePathMid: 'LMStudio_',
    ext: '.AppImage'
  }
};

export default otherApps;
