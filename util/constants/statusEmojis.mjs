export const statusEmo = {
  check: '\u{2705}',
  fire: '\u{1F525}',
  ex: '\u{274C}',
  package: '\u{1F4E6}',
  snake: '\u{1F40D}',
  doc: '\u{1F4DC}',
  shell: '\u{1F41A}',
  up: '\u{2B06}'
};
