import { exec as execute } from 'child_process';
import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';
import util from 'util';
const exec = util.promisify(execute);

export const runShellCmd = (cmd) => {
  return new Promise((resolve, reject) => {
    execute(cmd, (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        reject(error);
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        reject(stderr);
      }
      console.log(`stdout: ${stdout}`);
      resolve(stdout);
    });
  });
};

export const killProcess = async (app) => {
  try {
    await runShellCmd(`killall ${app}`);
    return true;
  } catch (error) {
    return false;
  }
};

export const sudoPrompt = async () => {
  const rl = readline.createInterface({ input, output });
  const sudo = await rl.question('Enter your sudo password: ');
  rl.close();
  return sudo;
};

export const startProcess = async (app) => {
  try {
    await runShellCmd(app);
    return true;
  } catch (error) {
    return false;
  }
};

export const runShellScript = async (cmd) => {
  try {
    const { stdout, stderr } = await exec(cmd);
    console.log('stdout:', stdout);
    console.log('stderr:', stderr);
  } catch (err) {
    console.error(err);
  }
};
