import { dirname, extname, join } from 'node:path';
import { readdirSync, unlinkSync } from 'node:fs';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const extensions = ['.zip', '.AppImage', '.gz', '.deb', '.7z', '.bin'];

export const deleteZipFiles = async () => {
  const dir = readdirSync(join(`${__dirname}`, '../downloads')) || [];
  const zipFiles = dir.filter((file) => extensions.indexOf(extname(file)) >= 0) || [];
  const zLen = zipFiles.length;
  if (zLen) {
    for (let i = 0; i < zLen; i++) {
      unlinkSync(join(__dirname, '../downloads', zipFiles[i]), (err) => {
        if (err) {
          console.log(err);
        }
      });
      if (i + 1 === zLen) return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
};

export async function deleteNewApps() {
  await deleteZipFiles();
}
