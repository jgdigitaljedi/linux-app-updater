import chalk from 'chalk';
import { PythonShell } from 'python-shell';
import config from '../config.json' assert { type: 'json' };
import { downloadFw } from './downloadNewFile.mjs';
import { getGhTags, getGithubData } from './githubHelper.mjs';
import { updateDlLinkList } from './updateDlLinkList.mjs';
import { writeUpdatedConfig } from './writeUpdatedConfig.mjs';
import { killProcess, runShellCmd, runShellScript, sudoPrompt } from './runShellCommands.mjs';
import { statusEmo } from './constants/statusEmojis.mjs';

function logCommand(updCmd) {
  if (updCmd.type === 'pip') {
    return `python3 -m pip install ${updCmd.name} -U`;
  }
  if (updCmd.type === 'shell') {
    return updCmd.name;
  }
  if (updCmd.type === 'shellArg') {
    return `${updCmd.beforeArg}${updCmd.afterArg}`;
  }
}

const updateCommands = async (app, version) => {
  return new Promise((resolve) => {
    const cmdInfo = app.updateCmd;
    if (cmdInfo.type === 'pip') {
      const options = {
        args: [cmdInfo.name]
      };
      PythonShell.run('./util/pipInstall.py', options)
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          console.log(error);
          resolve({ error });
        });
    } else if (cmdInfo.type === 'shell') {
      if (app.sudo) {
        sudoPrompt().then((pwd) => {
          runShellCmd(`echo ${pwd} | sudo -S ${cmdInfo.name}`)
            .then((result) => resolve(result))
            .catch(() => resolve()); // ignoring because will be sudo prompt
        });
      } else {
        runShellCmd(cmdInfo.name)
          .then((result) => resolve(result))
          .catch((error) => {
            if (app.itemName === 'Deno') {
              resolve(error);
            }
            resolve({ error });
          });
      }
    } else if (cmdInfo.type === 'shellArg') {
      const command = `${cmdInfo.beforeArg}${version}${cmdInfo.afterArg}`;
      runShellCmd(command)
        .then((result) => resolve(result))
        .catch(() => resolve()); // ignoring because will be stdout
    } else if (cmdInfo.type === 'shellScript') {
      runShellScript(cmdInfo.name)
        .then((result) => resolve(result))
        .catch((error) => {
          error;
        });
    }
  });
};

const dlMessageSymbol = (updCmd) => {
  if (!updCmd) {
    return statusEmo.package;
  }
  if (updCmd.type === 'pip') {
    return statusEmo.snake;
  }
  if (updCmd.type === 'shellScript') {
    return statusEmo.doc;
  }
  if (updCmd.type === 'shellArg' || updCmd.type === 'shell') {
    return statusEmo.shell;
  }
  return '';
};

const genericGhCall = async ({ owner, repo, configKey, itemName }, dlIndex) => {
  try {
    const { latest, dlLink } = await getGithubData(owner, repo);

    if (config[configKey].version !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${
            statusEmo.up
          } ${itemName.toUpperCase()} VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList(configKey, dlLink);
      return { name: configKey, link: dlLink, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} ${itemName} is currently up to date.`));
      return null;
    }
  } catch (error) {
    console.log('owner', owner);
    return { error };
  }
};

export const genericGhUpdate = async (app, isTest, dlIndex) => {
  try {
    if (app.ext) {
      const newVersion = await genericGhCall(app, dlIndex || 0);
      if (newVersion && !newVersion.error) {
        await writeUpdatedConfig(app.configKey, newVersion.version);
        if (app.ext) {
          await downloadFw(
            newVersion,
            `../downloads/${app.filePathMid}${newVersion.version}${app.ext}`,
            `${statusEmo.fire} Error downloading latest version of ${app.itemName}`
          );
          console.log(
            chalk.magenta(`${statusEmo.package} Downloaded latest version of ${app.itemName}!\n`)
          );
        } else {
          console.log(
            chalk.magenta.bold(
              `${statusEmo.up} THERE IS AN UPDATE AVAILABLE FOR ${app.itemName}!\n`
            )
          );
        }
      }
    } else {
      const newVersion = await getGhTags(app);
      if (config[app.configKey].version !== newVersion.latest && !!app.updateCmd) {
        console.log(
          chalk.yellow.bold(
            `\n${statusEmo.up} ${app.itemName.toUpperCase()} VERSION UPDATE AVAILABLE (${
              newVersion.latest
            }): ${newVersion.dlLink}`
          )
        );

        let xkilled = true;
        if (app.processName && !isTest) {
          xkilled = await killProcess(app.processName);
        }

        if (!xkilled && !isTest) {
          console.log(
            chalk.red.bold(
              `${statusEmo.ex} ${app.itemName} process could not be killed and therefore will not be updated automatically!`
            )
          );
          console.log(
            chalk.magenta.italic(
              `Try running this manually:\nxkill ${app.processName}\n${logCommand(app.updateCmd)}`
            )
          );
        }

        let updated;
        if (isTest) {
          updated = await Promise.resolve();
        } else {
          updated = await updateCommands(app, newVersion.latest);
        }

        if (updated?.error) {
          console.log(
            chalk.red.bold(
              `${statusEmo.fire} There was an error running update command for ${app.itemName}`,
              updated.error
            )
          );
        } else {
          if (app.startCmd && !isTest) {
            xkilled = await startProcessProcess(app.startCmd);
          }
          console.log(
            chalk.yellow(
              `${dlMessageSymbol(app.updateCmd)} ${app.itemName} was updated to version ${
                newVersion.latest
              }`
            )
          );
          await updateDlLinkList(app.configKey, newVersion.dlLink);
          await writeUpdatedConfig(app.configKey, newVersion.latest);
          return newVersion;
        }
      } else if (config[app.configKey].version !== newVersion.latest) {
        console.log(
          chalk.yellow.bold(
            `\n${statusEmo.up} ${app.itemName.toUpperCase()} VERSION UPDATE AVAILABLE (${
              newVersion.latest
            }): ${newVersion.dlLink}`
          )
        );
        await updateDlLinkList(app.configKey, newVersion.dlLink);
        await writeUpdatedConfig(app.configKey, newVersion.version);
        await downloadFw(
          newVersion,
          `../downloads/${app.filePathMid}${newVersion.version}${app.ext}`,
          `${statusEmo.fire} Error downloading latest version of ${app.itemName}`
        );
      } else {
        console.log(chalk.cyan(`${statusEmo.check} ${app.itemName} is currently up to date.`));
      }
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${app.itemName}`));
    console.error(error);
  }
};
