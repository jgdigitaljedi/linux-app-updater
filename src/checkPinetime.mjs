import chalk from 'chalk';
import genericGhApps from '../util/constants/genericGhApps.mjs';
import { getGithubData } from '../util/githubHelper.mjs';
import config from '../config.json' assert { type: 'json' };
import { updateDlLinkList } from '../util/updateDlLinkList.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { statusEmo } from '../util/constants/statusEmojis.mjs';

const ptCheckLogic = async (app) => {
  try {
    const { latest, assets } = await getGithubData(app.owner, app.repo, true);
    const dlLink = assets
      .map((item) => {
        return {
          name: item.name,
          link: item.browser_download_url
        };
      })
      .find((item) => item.name.includes('pinetime'));

    if (config.pinetime.version !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} PINETIME FIRMWARE UPDATE AVAILABLE (${latest}): ${dlLink.link}`
        )
      );
      await updateDlLinkList(app.configKey, dlLink.link);
      return { name: app.configKey, version: latest, dlList: dlLink };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Pinetime firmware is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
};

const checkPinetime = async () => {
  try {
    const app = genericGhApps.pinetime;
    const newFw = await ptCheckLogic(app);
    if (newFw && !newFw.error) {
      await writeUpdatedConfig(app.configKey, newFw.version);
      const download = newFw.dlList;
      await downloadFw(
        download,
        `../downloads/${app.filePathMid}_${download.name}`,
        `${statusEmo.fire} Error downloading latest version of PineTime firmware`
      );
      console.log(
        chalk.magenta(
          `${statusEmo.package} Downloaded file for latest version of PineTime firmware!\n`
        )
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for PineTime firmware`)
    );
  }
};

export default checkPinetime;
