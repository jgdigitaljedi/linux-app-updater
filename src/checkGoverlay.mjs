import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkGoverlay = async () => {
  const app = genericGhApps.goverlay;
  await genericGhUpdate(app);
};

export default checkGoverlay;