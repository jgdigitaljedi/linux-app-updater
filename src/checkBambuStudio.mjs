import chalk from 'chalk';
import genericGhApps from '../util/constants/genericGhApps.mjs';
import { getGithubData } from '../util/githubHelper.mjs';
import config from '../config.json' assert { type: 'json' };
import { updateDlLinkList } from '../util/updateDlLinkList.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { statusEmo } from '../util/constants/statusEmojis.mjs';

const bambuCheckLogic = async (app, which) => {
  try {
    const { latest, assets } = await getGithubData(app.owner, app.repo, true);
    const dlLink = assets
      .map((item) => {
        return {
          name: item.name,
          link: item.browser_download_url
        };
      })
      .find((item) => item.name.includes(app.term));

    if (which !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} BAMBU STUDIO UPDATE AVAILABLE (${latest}): ${dlLink.link}`
        )
      );
      await updateDlLinkList(app.configKey, dlLink.link);
      return { name: app.configKey, version: latest, dlList: dlLink };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Bambu Studio is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
};

const checkBambu = async (isUbuntu) => {
  try {
    const app = isUbuntu ? genericGhApps.bambuStudio_Ubuntu : genericGhApps.bambuStudio_Fedora;
    const variant = isUbuntu
      ? config.bambuStudio_Ubuntu.version
      : config.bambuStudio_Fedora.version;
    const newFw = await bambuCheckLogic(app, variant);
    if (newFw && !newFw.error) {
      await writeUpdatedConfig(app.configKey, newFw.version);
      const download = newFw.dlList;
      await downloadFw(
        download,
        `../downloads/${app.filePathMid}_${download.name}`,
        `${statusEmo.fire} Error downloading latest version of Bambu Studio`
      );
      console.log(
        chalk.magenta(`${statusEmo.package} Downloaded file for latest version of Bambu Studio!\n`)
      );
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for Bambu Studio`));
  }
};

export default checkBambu;
