import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkDeno = async () => {
  const device = genericGhApps.deno;
  await genericGhUpdate(device);
};

export default checkDeno;
