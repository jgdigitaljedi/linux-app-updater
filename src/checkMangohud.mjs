import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkMangohud = async () => {
  const app = genericGhApps.mangohud;
  await genericGhUpdate(app);
};

export default checkMangohud;
