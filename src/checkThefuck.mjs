import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkThefuck = async () => {
  const device = genericGhApps.thefuck;
  await genericGhUpdate(device);
};

export default checkThefuck;
