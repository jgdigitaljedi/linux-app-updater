import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkSdkman = async () => {
  const device = genericGhApps.sdkman;
  await genericGhUpdate(device);
};

export default checkSdkman;
