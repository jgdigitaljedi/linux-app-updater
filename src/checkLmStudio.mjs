import { scrapeSite } from '../util/scraperHelper.mjs';
import config from '../config.json' assert { type: 'json' };
import chalk from 'chalk';
import { statusEmo } from '../util/constants/statusEmojis.mjs';
import otherApps from '../util/constants/otherApps.mjs';
import { updateDlLinkList } from '../util/updateDlLinkList.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';

const getLmStudioCurrentVersion = async (app) => {
  try {
    const $ = await scrapeSite('https://lmstudio.ai/');
    const allPs = $('p');
    const versionButtonText = $(allPs).filter((i, el) =>
      $(el).text().includes('Download LM Studio for Linux')
    );
    const versionButton = $(versionButtonText).closest('button');
    const versionText = $(versionButton).find('p').last().text();
    if (versionText && versionText.trim() !== config.lmstudio.version) {
      // versions mismatch so get DL link
      const dlLink = $(versionButton).closest('a').attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} LM STUDIO UPDATE AVAILABLE (${versionText}): ${dlLink}`
        )
      );
      await updateDlLinkList(app.configKey, dlLink);
      return { version: versionText, downloadLink: dlLink };
    } else if (versionText) {
      console.log(chalk.cyan(`${statusEmo.check} LM Studio is currently up to date.`));
      // versions match so no need to DL
      return { version: versionText, downloadLink: null };
    }
    console.log(chalk.magenta(`\n${statusEmo.warn} LM Studio version could not be determined.`));
    return null;
  } catch (error) {
    console.log(chalk.red.bold('Error fetching LM Studio version:', error));
    return null;
  }
};

const checkLmStudio = async () => {
  try {
    const app = otherApps.lmstudio;
    const versionInfo = await getLmStudioCurrentVersion(app);
    if (versionInfo?.downloadLink) {
      // download the new version and update config.json
      await downloadFw(
        { link: versionInfo.downloadLink },
        `../downloads/${app.filePathMid}_${versionInfo.version}.${app.ext}`,
        `${statusEmo.fire} Error downloading latest version of LM Studio`
      );
      await writeUpdatedConfig(app.configKey, versionInfo.version);
      console.log(
        chalk.magenta(`${statusEmo.package} Downloaded file for latest version of LM Studio!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for LM Studio`, error)
    );
  }
};

export default checkLmStudio;
