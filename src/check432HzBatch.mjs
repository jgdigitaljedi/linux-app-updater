import chalk from 'chalk';
import genericGhApps from '../util/constants/genericGhApps.mjs';
import { getGithubData } from '../util/githubHelper.mjs';
import config from '../config.json' assert { type: 'json' };
import { updateDlLinkList } from '../util/updateDlLinkList.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { statusEmo } from '../util/constants/statusEmojis.mjs';

const batchConCheckLogic = async (app) => {
  try {
    const { latest, assets } = await getGithubData(app.owner, app.repo, true);
    const dlLink = assets
      .map((item) => {
        return {
          name: item.name,
          link: item.browser_download_url
        };
      })
      .filter((item) => item.name.includes('Converter432Hz'))
      .find((item) => item.name.includes('Linux_x64.AppImage'));
    if (config.batch432.version !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} 432HZ BATCH CONVERTOR UPDATE AVAILABLE (${latest}): ${dlLink.link}`
        )
      );
      await updateDlLinkList(app.configKey, dlLink.link);
      return { name: app.configKey, version: latest, dlList: dlLink };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} 432Hz Batch Convertor is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
};

const check432HzBatchConvertor = async () => {
  try {
    const app = genericGhApps.batch432;
    const newFw = await batchConCheckLogic(app);
    if (newFw && !newFw.error) {
      await writeUpdatedConfig(app.configKey, newFw.version);
      const download = newFw.dlList;
      await downloadFw(
        download,
        `../downloads/${app.filePathMid}_${download.name}`,
        `${statusEMo.fire} Error downloading latest version of 432Hz Batch Convertor`
      );
      console.log(
        chalk.magenta(
          `${statusEmo.package} Downloaded file for latest version of 432Hz Batch Convertor!\n`
        )
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for 432Hz Batch Convertor`)
    );
  }
};

export default check432HzBatchConvertor;
