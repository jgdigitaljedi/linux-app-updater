import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkNvm = async () => {
  const device = genericGhApps.nvm;
  await genericGhUpdate(device);
};

export default checkNvm;