import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkStreamdeck = async () => {
  const device = genericGhApps.streamdeckGUI;
  await genericGhUpdate(device);
};

export default checkStreamdeck;
