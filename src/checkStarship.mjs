import genericGhApps from '../util/constants/genericGhApps.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkStarship = async () => {
  const device = genericGhApps.starship;
  await genericGhUpdate(device);
};

export default checkStarship;