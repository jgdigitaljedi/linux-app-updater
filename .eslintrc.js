module.exports = {
  env: {
    es2021: true,
    node: true,
    'jest/globals': true
  },
  extends: ['eslint:recommended', 'prettier'],
  plugins: ['prettier', 'jest'],
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
    babelOptions: {
      plugins: ['@babel/plugin-syntax-import-assertions']
    }
  },
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    'no-prototype-builtins': ['off']
  }
};
